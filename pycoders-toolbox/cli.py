import click

@click.command()
def main():
    print("A strong Toolbox for Python Coders")

if __name__ == '__main__':
    main()