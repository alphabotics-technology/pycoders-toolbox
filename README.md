# PyCoders Toolbox

Python Command Line Toolbox for generate python project quickly.

## Development locally

Create virtual environment

```
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.in
```

Test it

```shell
python src/cli.py
```

## Setup locally

```shell
pip install --editable ./src
```

## Upload to PiPy

1. First, setup 

```shell
python -m keyring set https://upload.pypi.org/legacy/ <your_username>
```

2. Create some distributions in the normal way:

```shell
python setup.py sdist bdist_wheel
```

Upload to TestPyPI by specifying the --repository flag:
```shell
twine upload --repository testpypi dist/*
```

Upload the latest version we just built

```shell
twine upload dist/pycoders-toolbox-x.x.x.tar.gz 
```